import express from "express";
import PingController from "../controllers/ping";
import Web3Controller from "../controllers/web3";


const router = express.Router();
const pingController = new PingController();
const web3Controller = new Web3Controller();

router.get("/ping",async (_req, res) => {
  const response = await pingController.getMessage();
  return res.send(response);
});

router.post("/ping", async (req:any, res) => {
  const response = await pingController.sendMessage(req.body);
  return res.send(response);
})

router.get("/web3/getBalance", async(req, res) =>{
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.getBalanceAddress(address);
  return res.send(response);
})

router.get("/web3/getBalanceSPL", async(req, res) =>{
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.getBalanceSPL(address);
  return res.send(response);
})

router.get("/web3/getListToken", async(req, res) => {
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.getListToken(address);
  return res.send(response);
})

router.get("/web3/getListNFT", async(req, res) => {
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.getListNFT(address);
  return res.send(response);
})

router.get("/web3/createWallet", async(_req, res) => {
  const response = await web3Controller.createWallet( );
  return res.send(response);
})

router.get("/web3/airdrop", async(req:any,res: any) => {
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.airdrop(address);
  return res.send(response);
})

router.get("/web3/transferspl", async(req:any,res: any) => {
  let address:any = req.query['address']?.toString()
  const response = await web3Controller.transfetSPL(address);
  return res.send(response);
})

router.get("/web3/transferspltotatarget", async(req:any,res: any) => {
  let secKey:any = req.query['fromSecretKey']?.toString()
  let address:any = req.query['toAddress']?.toString()
  const response = await web3Controller.transfetSPLToTarget(secKey,address);
  return res.send(response);
})
router.get("/web3/transfersol", async(req:any,res: any) => {
  let from:any = req.query['fromSecretKey']?.toString()
  let to:any = req.query['toAddress']?.toString()
  const response = await web3Controller.transferSOL(from,to);
  return res.send(response);
})

router.get("/web3/importWallet", async(req:any,res: any) => {
  let from:any = req.query['fromSecretKey']?.toString()
  const response = await web3Controller.importwallet(from);
  let a= 0;
  return res.send(response);
})

router.get("/web3/importphrase", async(req:any,res: any) => {
  let phrase:any = req.query['phrase']?.toString()
  const response = await web3Controller.importphrase(phrase);
  return res.send(response);
})

export default router;