import express, {Application} from "express";
import morgan from "morgan";
import Router from "./routes";
import swaggerUi from "swagger-ui-express";
import cors from 'cors'

const PORT = process.env.PORT || 8000;

const app: Application = express()
app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static("public"));


// const allowedOrigins = ['http://localhost:3000'];

// const options: cors.CorsOptions = {
//   origin: allowedOrigins
// };
// app.use(cors(options));

app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
      swaggerOptions: {
        url: "/swagger.json",
      },
    })
  );
app.use(Router);


app.listen(PORT, () => {
    console.log("Server running on port = ",PORT);
})