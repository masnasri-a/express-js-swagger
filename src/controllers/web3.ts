import { Body, Get, Path, Post, Query, Route } from "tsoa";
import * as solanaWeb3 from "@solana/web3.js";
import * as bip39 from "bip39";
import {
  getOrCreateAssociatedTokenAccount,
  TOKEN_PROGRAM_ID,
  transfer,
} from "@solana/spl-token";
import { GetProgramAccountsFilter } from "@solana/web3.js";
import { getParsedNftAccountsByOwner } from "@nfteyez/sol-rayz";

require("dotenv").config();

interface ResponseModel {
  status: string;
  data: any;
}
interface ResponseModelCreateWallet {
  status: string;
  data: any;
  secretKey: any;
}
interface Pubkey {
  publicKey: string;
}

const secretKey = [
  226, 30, 30, 219, 25, 147, 41, 97, 246, 230, 234, 159, 34, 75, 3, 17, 221,
  107, 86, 20, 197, 54, 136, 250, 103, 44, 114, 10, 250, 199, 76, 51, 32, 109,
  158, 25, 225, 114, 181, 42, 160, 253, 109, 11, 177, 216, 231, 196, 211, 114,
  164, 77, 18, 140, 51, 122, 116, 55, 48, 102, 136, 66, 160, 240,
];

type Datas = Pick<Pubkey, "publicKey">;

const transferSPL = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let connection = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let fromWallet = solanaWeb3.Keypair.fromSecretKey(Uint8Array.from(secretKey));
  let mint = new solanaWeb3.PublicKey(
    "AHHJLLAsf9KiKxAmnVeLvXAbCo13LdAgUUaSEWXskooR"
  );
  const fromTokenAccount = await getOrCreateAssociatedTokenAccount(
    connection,
    fromWallet,
    mint,
    fromWallet.publicKey
  );
  let targetPubkey = new solanaWeb3.PublicKey(address);
  const toTokenAccount = await getOrCreateAssociatedTokenAccount(
    connection,
    fromWallet,
    mint,
    targetPubkey
  );
  let tx = await transfer(
    connection,
    fromWallet,
    fromTokenAccount.address,
    toTokenAccount.address,
    fromWallet.publicKey,
    1000000000
  );
  console.log(tx);
};

const transferSPLToOther = async (secKey: string, address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let connection = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let secsKey = secKey.slice(1, -1).split(",").map(Number);
  let Uint8Arrays = Uint8Array.from(secsKey);
  let fromWallet = solanaWeb3.Keypair.fromSecretKey(Uint8Arrays);
  let mint = new solanaWeb3.PublicKey(
    "AHHJLLAsf9KiKxAmnVeLvXAbCo13LdAgUUaSEWXskooR"
  );
  let fromWalletSigner = solanaWeb3.Keypair.fromSecretKey(
    Uint8Array.from(secretKey)
  );
  const fromTokenAccount = await getOrCreateAssociatedTokenAccount(
    connection,
    fromWallet,
    mint,
    fromWallet.publicKey
  );

  let targetPubkey = new solanaWeb3.PublicKey(address);

  console.log("from account " + fromWallet.publicKey.toBase58());
  console.log("=================");
  console.log("target account " + targetPubkey);
  console.log("=================");
  const toTokenAccount = await getOrCreateAssociatedTokenAccount(
    connection,
    fromWallet,
    mint,
    targetPubkey
  );
  console.log("token account = " + toTokenAccount);
  console.log("=================");
  let tx = await transfer(
    connection,
    fromWallet,
    fromTokenAccount.address,
    toTokenAccount.address,
    fromWallet.publicKey,
    1000000000
  );
  console.log(tx);
};

const getBalanceSPL = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let connection = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let fromWallet = solanaWeb3.Keypair.fromSecretKey(Uint8Array.from(secretKey));
  let mint = new solanaWeb3.PublicKey(
    "AHHJLLAsf9KiKxAmnVeLvXAbCo13LdAgUUaSEWXskooR"
  );
  let targetPubkey = new solanaWeb3.PublicKey(address);
  const test = await getOrCreateAssociatedTokenAccount(
    connection,
    fromWallet,
    mint,
    targetPubkey
  );
  console.log("Amount = " + test.amount);

  return Number(test.amount);
};

const checkConnection = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let connection = new solanaWeb3.Connection(rpcUrl, "confirmed");
  return await connection.getBalance(new solanaWeb3.PublicKey(address));
};

const getListTokenData = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let connection = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let wallet = new solanaWeb3.PublicKey(address);
  const filters: GetProgramAccountsFilter[] = [
    {
      dataSize: 165, //size of account (bytes)
    },
    {
      memcmp: {
        offset: 32, //location of our query in the account (bytes)
        bytes: wallet.toString(), //our search criteria, a base58 encoded string
      },
    },
  ];
  const accounts = await connection.getParsedProgramAccounts(TOKEN_PROGRAM_ID, {
    filters: filters,
  });
  let arrayToken: any = [];
  accounts.forEach((account, index) => {
    const data: any = account.account.data;
    const mint: string = data["parsed"]["info"]["mint"];
    const balance: any = data["parsed"]["info"]["tokenAmount"]["uiAmount"];
    arrayToken.push({
      mint: mint,
      balance: balance,
    });
  });
  return arrayToken;
};

const getListNFT = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let conn = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let public_key: any = new solanaWeb3.PublicKey(address);
  const nftArray = await getParsedNftAccountsByOwner({
    publicAddress: public_key,
    connection: conn,
  });
  return nftArray;
};

const createWallet = async () => {
  let keypair = solanaWeb3.Keypair.generate();
  return keypair;
};

const airdrop = async (address: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let conn = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let airdropSignature = await conn.requestAirdrop(
    new solanaWeb3.PublicKey(address),
    solanaWeb3.LAMPORTS_PER_SOL * 2
  );
  await conn.confirmTransaction(airdropSignature);
  return 2;
};

const importWallets = async (from: string) => {
  try {
    // let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
    // let conn = new solanaWeb3.Connection(rpcUrl, "confirmed");
    let data = from
      .slice(0, -1)
      .replace('"', "")
      .replace("[", "")
      .replace("]", "")
      .replace(" ", "");
    console.log(data);

    var keypairs = data.split(",").map(Number);
    let keypair = solanaWeb3.Keypair.fromSecretKey(Uint8Array.from(keypairs));
    let pubKey = keypair.publicKey.toString();
    return {
      keypair: keypair.secretKey.toString(),
      pubkey: pubKey,
    };
  } catch {
    return 0;
  }

  // let secretKey = Uint8Array.from(from.replace('"',""));
};

const sendSOL = async (from: string, to: string) => {
  let rpcUrl = process.env.RPC_URL || "https://api.devnet.solana.com";
  let conn = new solanaWeb3.Connection(rpcUrl, "confirmed");
  let transaction = new solanaWeb3.Transaction();
  let secsKey = from.slice(1, -1).split(",").map(Number);
  let Uint8Arrays = Uint8Array.from(secsKey);
  let fromWallet = solanaWeb3.Keypair.fromSecretKey(Uint8Arrays);
  let toKey = new solanaWeb3.PublicKey(to);
  transaction.add(
    solanaWeb3.SystemProgram.transfer({
      fromPubkey: fromWallet.publicKey,
      toPubkey: toKey,
      lamports: solanaWeb3.LAMPORTS_PER_SOL,
    })
  );
  await solanaWeb3.sendAndConfirmTransaction(conn, transaction, [fromWallet]);
};

const importPhrase = async (phrase: string) => {
  try {
    const seed = bip39.mnemonicToSeedSync(phrase).slice(0, 32);
    const Keypair = solanaWeb3.Keypair.fromSeed(seed);
    return {
      keypair: Keypair.secretKey.toString(),
      pubkey: Keypair.publicKey.toBase58(),
    };
  } catch { 
    return 0;
  }

  // const seedBuffer = Buffer.from(seed).toString('hex');
};

@Route("web3")
export default class Web3Controller {
  @Get("/getBalance")
  public async getBalanceAddress(
    @Query() address: string
  ): Promise<ResponseModel> {
    try {
      let balance = await checkConnection(address);
      return {
        status: "success",
        data: balance / 1000000000,
      };
    } catch (err) {
      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/getBalanceSPL")
  public async getBalanceSPL(@Query() address: string): Promise<ResponseModel> {
    try {
      let balance = await getBalanceSPL(address);
      return {
        status: "success",
        data: balance / 1000000000,
      };
    } catch (err) {
      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/getListToken")
  public async getListToken(@Query() address: string): Promise<ResponseModel> {
    try {
      let data = await getListTokenData(address);
      return {
        status: "success",
        data: data,
      };
    } catch (err) {
      return {
        status: "success",
        data: "data not found",
      };
    }
  }

  @Get("/getListNFT")
  public async getListNFT(@Query() address: string): Promise<ResponseModel> {
    try {
      let data = await getListNFT(address);
      return {
        status: "success",
        data: data,
      };
    } catch (err) {
      return {
        status: "success",
        data: "data not found",
      };
    }
  }

  @Get("/createWallet")
  public async createWallet(): Promise<ResponseModelCreateWallet> {
    try {
      let data = await createWallet();
      console.log(data.secretKey.toString());
      return {
        status: "success",
        data: data.publicKey.toBase58(),
        secretKey: data.secretKey.toString(),
      };
    } catch (error) {
      console.log(error);

      return {
        status: "success",
        data: "data not found",
        secretKey: "data not found",
      };
    }
  }

  @Get("/airdrop")
  public async airdrop(@Query() address: string): Promise<ResponseModel> {
    try {
      let airdrops = await airdrop(address);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/transferspl")
  public async transfetSPL(@Query() address: string): Promise<ResponseModel> {
    try {
      let airdrops = await transferSPL(address);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/transferspltotatarget")
  public async transfetSPLToTarget(
    @Query() fromSecretKey: string,
    @Query() toAddress: string
  ): Promise<ResponseModel> {
    try {
      let airdrops = await transferSPLToOther(fromSecretKey, toAddress);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      console.log(err);

      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/transfersol")
  public async transferSOL(
    @Query() fromSecretKey: string,
    @Query() toAddress: string
  ): Promise<ResponseModel> {
    try {
      let airdrops = await sendSOL(fromSecretKey, toAddress);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      return {
        status: "failed",
        data: 0,
      };
    }
  }

  @Get("/importwallet")
  public async importwallet(
    @Query() fromSecretKey: string
  ): Promise<ResponseModel> {
    try {
      let airdrops = await importWallets(fromSecretKey);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      return {
        status: "failed",
        data: [],
      };
    }
  }

  @Get("/importphrase")
  public async importphrase(@Query() phrase: string): Promise<ResponseModel> {
    try {
      let airdrops = await importPhrase(phrase);
      return {
        status: "success",
        data: airdrops,
      };
    } catch (err) {
      return {
        status: "failed",
        data: [],
      };
    }
  }
}
