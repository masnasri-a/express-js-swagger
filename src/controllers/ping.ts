import { Body, Get, Post, Route } from "tsoa";

interface PingResponse {
  message: string;
}

type Datas = Pick<PingResponse, "message">;

@Route("ping")
export default class PingController {
  @Get("/")
  public async getMessage(): Promise<PingResponse> {
    return {
      message: "pong",
    };
  }

  //   @SuccessResponse("201", "Created")
  @Post("/")
  public async sendMessage(@Body() data: Datas): Promise<any> {
    try {
      console.log("data = ",data.message);
      return {
        message: data.message,
      };
    } catch (err) {
      console.log("error = ", err);
    }
  }
}

